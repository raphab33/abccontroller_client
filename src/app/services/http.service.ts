import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class HttpService {
  constructor(private http: HttpClient) {}

  buscarTodos(endpoint) {
    return this.http.get(`${environment.apiUrl}${endpoint}`);
  }

  buscarByCod(cod, endpoint) {
    return this.http.get(`${environment.apiUrl}${endpoint}`, cod);
  }

  adicionar(object: any, endpoint) {
    return this.http.post<any>(`${environment.apiUrl}${endpoint}`, object);
  }

  deletar(cod: number, endpoint) {
    return this.http.delete(`${environment.apiUrl}${endpoint}${cod}/`);
  }

  atualizar(object: any, endpoint) {
    return this.http.put(
      `${environment.apiUrl}${endpoint}${object.cod}/`,
      object
    );
  }
  atualizarParcialmente(cod, object: any, endpoint) {
    return this.http.patch(`${environment.apiUrl}${endpoint}${cod}/`, object);
  }
}
