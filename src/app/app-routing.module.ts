import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    loadChildren: () =>
      import("./core/home/home.module").then(mod => mod.HomeModule)
  },
  {
    path: "aluno",
    loadChildren: () =>
      import("./core/aluno/aluno.module").then(mod => mod.AlunoModule)
  },
  {
    path: "usuario",
    loadChildren: () =>
      import("./core/usuario/usuario.module").then(mod => mod.UsuarioModule)
  },
  {
    path: "endereco",
    loadChildren: () =>
      import("./core/endereco/endereco.module").then(mod => mod.EnderecoModule)
  },
  {
    path: "curso",
    loadChildren: () =>
      import("./core/curso/curso.module").then(mod => mod.CursoModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
