export enum ENDPOINTS {
  aluno = "aluno/",
  usuario = "usuario/",
  endereco = "endereco/",
  curso = "curso/"
}
