import { ENDPOINTS } from "./../../interfaces/endpoint.interface";
import { HttpService } from "./../../services/http.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-usuario",
  templateUrl: "./usuario.component.html",
  styleUrls: ["./usuario.component.css"]
})
export class UsuarioComponent implements OnInit {
  usuarios: any[] = [];

  constructor(private http: HttpService) {}

  ngOnInit() {
    this.listarUsuarios();
  }

  listarUsuarios() {
    this.http.buscarTodos(ENDPOINTS.usuario).subscribe((res: any) => {
      console.log(res);
      this.usuarios = res;
    });
  }

  deletarUsuario(id) {
    this.http.deletar(id, ENDPOINTS.usuario).subscribe(() => {});
  }
}
