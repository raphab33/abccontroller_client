import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CursoComponent } from './curso.component';


const routes:Routes = [
  {
    path: "",
    component: CursoComponent
  }
]

@NgModule({
  declarations: [CursoComponent],
  imports: [CommonModule, RouterModule.forChild(routes)]
})
export class CursoModule { }
