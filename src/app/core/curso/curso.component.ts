import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ENDPOINTS } from 'src/app/interfaces/endpoint.interface';

@Component({
  selector: 'app-curso',
  templateUrl: './curso.component.html',
  styleUrls: ['./curso.component.css']
})
export class CursoComponent implements OnInit {

  cursos: any[] = [];
  constructor(private http: HttpService) {}
  
  ngOnInit(){
    this.listarCursos();
  }

  listarCursos(){
    this.http.buscarTodos(ENDPOINTS.curso).subscribe((res:any)=>{
      console.log(res);
      this.cursos = res;
    });
  }

  deletarCurso(nu_seq_curso){
    this.http.deletar(nu_seq_curso, ENDPOINTS.curso).subscribe(()=>{});
  }
}
