import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ENDPOINTS } from 'src/app/interfaces/endpoint.interface';

@Component({
  selector: 'app-endereco',
  templateUrl: './endereco.component.html',
  styleUrls: ['./endereco.component.css']
})
export class EnderecoComponent implements OnInit {
  enderecos: any[] = [];

  constructor(private http: HttpService) {}

  ngOnInit() {
    this.listarEnderecos();
  }

  listarEnderecos(){
    this.http.buscarTodos(ENDPOINTS.endereco).subscribe((res: any) =>{
      console.log(res);
      this.enderecos = res;
    });
  }

  deletarEndereco(nu_seq_endereco){
    this.http.deletar(nu_seq_endereco, ENDPOINTS.endereco).subscribe(()=>{});
  }

  atualizarEndereco(nu_seq_endereco){
    this.http.atualizar(nu_seq_endereco, ENDPOINTS.endereco).subscribe(()=>{});
  }

}
