import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { EnderecoComponent } from './endereco.component';

const routes:Routes = [
  {
    path: "",
    component: EnderecoComponent
  }
]

@NgModule({
  declarations: [EnderecoComponent],
  imports: [CommonModule, RouterModule.forChild(routes)]
})
export class EnderecoModule {}
